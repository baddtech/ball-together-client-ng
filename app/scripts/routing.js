BallTogether.module('BallTogether.routing', {
  depends: [
    'BallTogether.views.myEvents',
  ],
}, function () {
  'use strict';

  return {
    go: function (name) {
      switch (name) {
        case 'home':
          BallTogether.views.myEvents.refresh();
          break;
      }
      document.body.setAttribute('data-route', name);
    },
  };
});
