BallTogether.module('BallTogether', function () {
  'use strict';

  return {
    debounce: (function () {
      var queue = {};
      return function (name, handler, delay) {
        clearTimeout(queue[name]);
        delay = delay || 4;
        queue[name] = setTimeout(handler, delay);
      };
    })(),
  };
});
