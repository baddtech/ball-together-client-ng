(function () {
  'use strict';

  var waiting = {};

  function overload(func, newFunc) {
    var old = func;
    return function () {
      if (newFunc.length === arguments.length) {
        return newFunc.apply(this, arguments);
      } else if (typeof old === 'function') {
        return old.apply(this, arguments);
      }
    };
  }

  function canResolve(namespace) {
    if (namespace.indexOf('.') !== -1) {
      var identifier = namespace.split(/\./g), path = window;
      for (var i = 0, j = identifier.length - 1; i < j; i++) {
        var currentPath = identifier[i];
        if (path[currentPath] === undefined) {
          return false;
        }
        path = path[currentPath];
      }
      if (path[identifier[i]] === undefined) {
        return false;
      } else {
        return true;
      }
    } else {
      if (window[namespace] === undefined) {
        return false;
      } else {
        return true;
      }
    }
  }

  var create = function (namespace, definition) {
    var module = definition(undefined),
        resolved, prop;
    if (namespace.indexOf('.') !== -1) {
      var identifier = namespace.split(/\./g),
          path = window;
      for (var i = 0, j = identifier.length - 1, currentPath; i < j; i++) {
        currentPath = identifier[i];
        if (path[currentPath] === undefined) {
          path[currentPath] = {};
        }
        path = path[currentPath];
      }
      currentPath = identifier[i];
      resolved = path[currentPath];
      if (resolved === undefined) {
        path[currentPath] = module;
      } else {
        for (prop in module) {
          if (module.hasOwnProperty(prop)) {
            resolved[prop] = module[prop];
          }
        }
      }
    } else {
      resolved = window[namespace];
      if (resolved === undefined) {
        window[namespace] = module;
      } else {
        for (prop in module) {
          if (module.hasOwnProperty(prop)) {
            resolved[prop] = module[prop];
          }
        }
      }
    }
  };
  create = overload(create, function (namespace, options, definition) {
    function retry() {
      create(namespace, options, definition);
    }
    var depends = options.depends;
    if (depends !== undefined) {
      for (var i = 0, j = depends.length; i < j; i++) {
        if (!canResolve(depends[i])) {
          for (var module in waiting) {
            if (module !== namespace && waiting[module].indexOf(namespace) !== -1) {
              if (depends.indexOf(module) !== -1) {
                console.error('Circular Dependency:', namespace, 'and', module);
                return;
              }
            }
          }
          waiting[namespace] = depends;
          setTimeout(retry, 4);
          return;
        }
      }
    }
    create(namespace, definition);
  });

  var waitFor = function (namespace, callback) {
    if (Array.isArray(namespace)) {
      for (var i = 0, j = namespace.length; i < j; i++) {
        if (!canResolve(namespace[i])) {
          setTimeout(function () {
            waitFor(namespace, callback);
          }, 4);
          return;
        }
      }
      callback();
    } else {
      if (canResolve(namespace)) {
        callback();
      } else {
        setTimeout(function () {
          waitFor(namespace, callback);
        }, 4);
      }
    }
  };
  create.waitFor = waitFor;

  create('BallTogether.module', function () {
    return create;
  });
})();
