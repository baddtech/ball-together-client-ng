(function () {
  function fire(type, detail) {
    var event;
    if (detail) {
      event = new CustomEvent(type, {
        detail: detail,
      });
    } else {
      event = new Event(type);
    }
    this.dispatchEvent(event);
  }
  window.fire = fire;
  document.fire = fire;
  HTMLElement.prototype.fire = fire;
  SVGSVGElement.prototype.fire = fire;
})();
