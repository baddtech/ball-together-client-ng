(function () {
    'use strict';

    var decode = JSON.parse;

    window.jsonEncode = JSON.stringify;
    window.jsonDecode = function (str) {
        var parsed;
        try {
            parsed = decode(str);
        } catch (e) {
            console.error('Failed to decode JSON ("' + e + '"): ' + str);
            return false;
        }
        return parsed;
    };
})();
