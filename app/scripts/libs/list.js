BallTogether.module('BallTogether.list', function () {
  'use strict';

  function forceUpdate(list) {
    list.lastKnownIndex = undefined;
    list.empty();
    list.refresh();
    list.synchronize();
  }

  function getItemHeight(list, item) {
    var row = list.row,
        style = row.style,
        content = list.content;
    style.visibility = 'hidden';
    content.appendChild(row);
    row.appendChild(item);
    var height = row.clientHeight;
    row.removeChild(item);
    content.removeChild(row);
    style.visibility = '';
    return height;
  }

  function pushItem(item) {
    this.empty(true);
    this.totalRows++;
    this.items.push(item);

    var height = getItemHeight(this, item),
        rowHeights = this.rowHeights,
        rowTops = this.rowTops,
        rowTop = rowTops[rowTops.length - 1] + rowHeights[rowHeights.length - 1],
        totalHeight = rowTop + height;

    rowTops.push(rowTop);
    rowHeights.push(height);
    this.content.style.height = totalHeight + 'px';
    this.lastKnownIndex = undefined;
    this.fire('scroll');
  }
  function unshiftItem(item) {
    var items = this.items,
        index = items.indexOf(item),
        rowHeights = this.rowHeights,
        rowTops = this.rowTops;

    if (index !== -1) {
      if (index !== 0) {
        this.lastKnownIndex = undefined;
        items.unshift(items.splice(index, 1)[0]);
        rowHeights.unshift(rowHeights.splice(index, 1)[0]);
        var totalHeight = 0;
        for (var i = 0, j = rowHeights.length; i < j; i++) {
          rowTops[i] = totalHeight;
          totalHeight += rowHeights[i];
        }
        this.empty(true);
        this.fire('scroll');
      }
    } else {
      this.totalRows++;
      items.unshift(item);
      var height = getItemHeight(this, item),
          rowTop = rowTops[rowTops.length - 1] + rowHeights[rowHeights.length - 1];
      rowTops.push(rowTop);
      rowHeights.push(height);
      this.content.style.height = rowTop + height + 'px';
    }
  }
  function removeItem(item) {
    var items = this.items,
        index = items.indexOf(item);
    if (index !== -1) {
      this.totalRows--;
      items.splice(index, 1);
      forceUpdate(this);
    }
  }
  function empty(soft) {
    var pool = this.pool,
        onScreen = this.onScreen,
        onScreenIndexes = this.onScreenIndexes;
    for (var i = 0, j = onScreenIndexes.length; i < j; i++) {
      var current = onScreenIndexes[i];
      pool.push(onScreen[current]);
      delete onScreen[current];
    }
    if (!soft) {
      this.content.innerHTML = '';
    }
    onScreenIndexes.splice(0);
  }
  function refresh() {
    var listHeight = this.clientHeight,
        rowHeights = this.rowHeights,
        rowTops = this.rowTops,
        content = this.content,
        items = this.items,
        row = this.row,
        count = items.length,
        style = row.style,
        totalHeight = 0,
        visibleRows;

    rowTops.splice(0, rowTops.length);
    rowHeights.splice(0, rowHeights.length);

    style.visibility = 'hidden';
    content.appendChild(row);
    for (var i = 0; i < count; i++) {
      var item = items[i];
      row.appendChild(item);
      var height = row.clientHeight;
      row.removeChild(item);
      rowTops[i] = totalHeight;
      rowHeights[i] = height;
      totalHeight += height;
      if (!visibleRows && totalHeight > listHeight) {
        visibleRows = i + 1;
      }
    }
    content.removeChild(row);
    style.visibility = '';
    content.style.height = totalHeight + 'px';
    if (visibleRows) {
      this.visibleRows = visibleRows;
    } else {
      this.visibleRows = count;
    }
  }
  function updateVisibleRows() {
    var listHeight = this.clientHeight,
        rowHeights = this.rowHeights,
        count = this.items.length,
        totalHeight = 0,
        visibleRows;
    for (var i = 0; i < count; i++) {
      totalHeight += rowHeights[i];
      if (totalHeight > listHeight) {
        visibleRows = i + 1;
        break;
      }
    }
    if (visibleRows) {
      this.visibleRows = visibleRows;
    } else {
      this.visibleRows = count;
    }
  }
  function getIndex() {
    var rowHeights = this.rowHeights,
        scrollTop = this.scrollTop,
        height = 0;
    for (var i = 0, j = rowHeights.length; i < j; i++) {
      height += rowHeights[i];
      if (height > scrollTop) {
        return i;
      }
    }
    return 0;
  }
  function synchronize() {
    var currentIndex = this.getIndex();
    if (currentIndex !== this.lastKnownIndex) {
      this.lastKnownIndex = currentIndex;

      var lastIndex = currentIndex + this.visibleRows + 10,
          totalRows = this.totalRows;
      if (lastIndex > totalRows) {
        lastIndex = totalRows;
      }
      currentIndex -= 10;
      if (currentIndex < 0) {
        currentIndex = 0;
      }
      var pool = this.pool,
          onScreen = this.onScreen,
          onScreenIndexes = this.onScreenIndexes,
          index = onScreenIndexes.length;
      while (index--) {
        var current = onScreenIndexes[index];
        if (current > lastIndex || current < currentIndex) {
          pool.push(onScreen[current]);
          onScreenIndexes.splice(index, 1);
          delete onScreen[current];
        }
      }
      var row = this.row,
          items = this.items,
          content = this.content,
          rowTops = this.rowTops,
          needsRebind = false;
      for (var i = currentIndex; i < lastIndex; i++) {
        if (!onScreen[i]) {
          var container;
          if (pool.length) {
            container = pool.pop();
            container.innerHTML = '';
          } else {
            container = row.cloneNode();
          }
          var style = container.style,
              value = 'translate3d(0, ' + rowTops[i] + 'px, 0)';
          style.transform = value;
          style.webkitTransform = value;
          container.appendChild(items[i]);
          if (container.parentElement !== content) {
            content.appendChild(container);
          }
          onScreenIndexes.push(i);
          onScreen[i] = container;
          needsRebind = true;
        }
      }
      if (needsRebind) {
        setTimeout(this.rebind, 4);
      }
    }
  }
  function onScroll() {
    cancelAnimationFrame(this.animationFrame);
    this.animationFrame = requestAnimationFrame(this.synchronize);
  }
  function onResize() {
    var self = this;
    BallTogether.debounce(this.id + 'resize', function () {
      self.lastKnownIndex = undefined;
      self.updateVisibleRows();
      self.fire('scroll');
    }, 250);
  }

  return {
    init: function (element, items, row, rebind) {
      if (!element.initialized) {
        element.initialized = true;

        element.row = row;
        element.pool = [];
        element.rowTops = [];
        element.items = items;
        element.onScreen = {};
        element.rowHeights = [];
        element.onScreenIndexes = [];
        element.totalRows = items.length;
        element.content = element.firstElementChild;

        element.empty = empty.bind(element);
        element.refresh = refresh.bind(element);
        element.getIndex = getIndex.bind(element);
        element.pushItem = pushItem.bind(element);
        element.removeItem = removeItem.bind(element);
        element.unshiftItem = unshiftItem.bind(element);
        element.synchronize = synchronize.bind(element);
        element.updateVisibleRows = updateVisibleRows.bind(element);

        if (rebind) {
          element.rebind = rebind.bind(element);
        }

        element.addEventListener('scroll', onScroll.bind(element));
        addEventListener('resize', onResize.bind(element));
        window.fire('list-resized', element);
      } else {
        element.empty();
        element.items = items;
        element.totalRows = items.length;
        element.lastKnownIndex = undefined;
        element.scrollTop = 0;
      }
      element.refresh();
      element.fire('scroll');
    },
  };
});
