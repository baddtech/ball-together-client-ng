BallTogether.module('BallTogether.select-input', function () {
  'use strict';

  function onClick(e) {
    e.stopPropagation();
    if (currentTarget) {
      onBlur();
    }
    currentTarget = this;
    var classList = this.querySelector('.menu').classList;
    if (classList.contains('open')) {
      onBlur();
    } else {
      classList.add('open');
      document.addEventListener('click', onBlur);
    }
  }
  function onBlur() {
    document.removeEventListener('click', onBlur);
    currentTarget.querySelector('.menu').classList.remove('open');
  }
  function onSelect(e) {
    e.stopPropagation();
    var menu = this.parentElement;
    menu.classList.remove('open');
    var input = menu.parentElement.querySelector('input');
    input.value = this.dataset.value;
    input.previousElementSibling.classList.add('floated');
    input.fire('select');
  }

  var currentTarget;

  return {
    bind: function (scope) {
      var inputs = scope.querySelectorAll('.select-input');
      for (var i = 0, j = inputs.length; i < j; i++) {
        var current = inputs[i];
        current.addEventListener('click', onClick);
        var options = current.querySelectorAll('li');
        for (var k = 0, l = options.length; k < l; k++) {
          var option = options[k];
          option.addEventListener('click', onSelect, true);
        }
      }
    },
  };
});
