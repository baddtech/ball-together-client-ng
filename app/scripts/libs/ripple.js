BallTogether.module('BallTogether.ripple', function () {
  'use strict';

  function handler(e) {
    var parentElement = div.parentElement;
    if (parentElement !== null) {
      parentElement.removeChild(div);
    }
    var bounds = this.getBoundingClientRect(),
        xPos, yPos, type = e.type;
    if (e.type === 'touchstart') {
      var touch = e.touches[0] || e.changedTouches[0];
      xPos = touch.pageX - bounds.left;
      yPos = touch.pageY - bounds.top;
    } else if (e.type === 'mousedown') {
      xPos = e.pageX - bounds.left;
      yPos = e.pageY - bounds.top;
    }
    div.classList.add('ripple-effect');
    var style = div.style,
        height = bounds.height;
    style.height = height;
    style.width = bounds.width;
    style.top = (yPos - (height / 2)) + 'px';
    style.left = (xPos - (height / 2)) + 'px';
    style.background = this.dataset['ripple'];
    this.appendChild(div);
    var self = this;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      if (div.parentElement === self) {
        self.removeChild(div);
      }
    }, 1000);
  }
  function debounceHandler(e) {
    if (window.TouchEvent && e instanceof TouchEvent) {
      this.removeEventListener('mousedown', debounceHandler);
    }
    if (!this._rippleDebounced) {
      this._rippleDebounced = true;
      handler.call(this, e);
    }
    var self = this;
    BallTogether.debounce('_rippleDebounced', function () {
      delete self._rippleDebounced;
    }, 250);
  }

  var timeout,
      div = document.createElement('div');

  return {
    bind: function (scope) {
      var ripples = scope.querySelectorAll('.ripple');
      for (var i = 0, j = ripples.length; i < j; i++) {
        var current = ripples[i];
        current.addEventListener('mousedown', debounceHandler);
        current.addEventListener('touchstart', debounceHandler);
      }
    },
    unbind: function (scope) {
      var ripples = scope.querySelectorAll('.ripple');
      for (var i = 0, j = ripples.length; i < j; i++) {
        var current = ripples[i];
        current.removeEventListener('mousedown', debounceHandler);
        current.removeEventListener('touchstart', debounceHandler);
      }
    },
  };
});
