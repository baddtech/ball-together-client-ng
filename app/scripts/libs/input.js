BallTogether.module('BallTogether.input', function () {
  'use strict';

  function onInput(e) {
    if (e.type === 'keydown') {
      if (e.keyCode === 13) {
        e.preventDefault(); // prevent submit
      }
    }
    if (this.value !== '') {
      var previousElementSibling = this.previousElementSibling;
      if (previousElementSibling) {
        previousElementSibling.classList.add('floated');
      }
    } else {
      var previousElementSibling = this.previousElementSibling;
      if (previousElementSibling) {
        previousElementSibling.classList.remove('floated');
      }
    }
  }

  return {
    bind: function (scope) {
      var inputs = scope.querySelectorAll('.input input');
      for (var i = 0, j = inputs.length; i < j; i++) {
        var current = inputs[i];
        current.addEventListener('input', onInput);
        current.addEventListener('change', onInput);
        current.addEventListener('keydown', onInput);
        current.addEventListener('keyup', onInput);
      }
    },
    unbind: function (scope) {
      var inputs = scope.querySelectorAll('.input input');
      for (var i = 0, j = inputs.length; i < j; i++) {
        var current = inputs[i];
        current.removeEventListener('input', onInput);
        current.removeEventListener('change', onInput);
        current.removeEventListener('keydown', onInput);
        current.removeEventListener('keyup', onInput);
      }
    },
  };
});
