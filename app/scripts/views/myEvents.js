BallTogether.module('BallTogether.views.myEvents', {
  depends: [
    'BallTogether.list',
  ],
}, function () {
  'use strict';

  var myEventsList = document.getElementById('myEventsList'),
      myEventsTemplate = document.getElementById('myEventsTemplate'),
      template = myEventsTemplate.innerHTML;

  myEventsTemplate.parentElement.removeChild(myEventsTemplate);

  return {
    refresh: function () {
      Ajax.request({
        url: 'https://secure22.win.hostgator.com/api_baddtech_com/AppServices.asmx/GetEvents',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        data: jsonEncode({
          _city: 'Edmonton',
          _country: 'Canada',
        }),
        json: true,
        processData: false,
      }).done(function (response) {
        var d = response.d;
        if (Array.isArray(d)) {
          var stamped = [];
          for (var i = 0, j = d.length; i < j; i++) {
            var current = d[i];

// City:"Edmonton"
// Country:"Canada"
// Description:"Semi competative competition, come out and ball hard!"
// EndTimeDate:"/Date(1459047600000)/"
// ExpLevel:2
// Location:"Meadows Rec Center"
// RunID:9
// RunType:"Public"
// StartTimeDate:"/Date(1459036800000)/"

            stamped.push(template.replace(/\[\[City\]\]/, escapeHtml(current.City))
                                 .replace(/\[\[Country\]\]/, escapeHtml(current.Country))
                                 .replace(/\[\[ExpLevel\]\]/, escapeHtml(current.ExpLevel.toString()))
                                 .replace(/\[\[StartTimeDate\]\]/, escapeHtml(current.StartTimeDate))
                                 .replace(/\[\[Location\]\]/, escapeHtml(current.Location)));
          }
          var row = document.createElement('div');
          row.classList.add('row');
          row.innerHTML = stamped.join('');
          BallTogether.list.init(myEventsList, Array.from(row.children), row);
        } else {
          console.error('get myEvents failed:', response);
        }
      }).fail(function (response) {
        console.error('get myEvents failed:', response);
      });
    },
  };
});
