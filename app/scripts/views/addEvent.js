BallTogether.module('BallTogether.views.addEvent', function () {
  'use strict';

  var addEventTime = document.getElementById('addEventTime'),
      addEventDate = document.getElementById('addEventDate'),
      addEventEventType = document.getElementById('addEventEventType'),
      addEventLocation = document.getElementById('addEventLocation'),
      addEventExperienceLevel = document.getElementById('addEventExperienceLevel'),
      addEventDescription = document.getElementById('addEventDescription'),
      addEventSubmit = document.getElementById('addEventSubmit');

  function disableSubmit() {
    addEventSubmit.setAttribute('disabled', true);
  }
  function enableSubmit() {
    addEventSubmit.removeAttribute('disabled');
  }

  function validate() {
    if (addEventTime.value === '') {
      disableSubmit();
    } else if (addEventDate.value === '') {
      disableSubmit();
    } else if (addEventEventType.value === '') {
      disableSubmit();
    } else if (addEventLocation.value === '') {
      disableSubmit();
    } else if (addEventExperienceLevel.value === '') {
      disableSubmit();
    } else if (addEventDescription.value === '') {
      disableSubmit();
    } else {
      enableSubmit();
    }
  }

  function selectExperienceLevel() {
    var value = parseInt(this.dataset.value, 10);
    for (var i = 0; i < value; i++) {
      experienceLevels[i].classList.add('enabled');
    }
    for (var j = experienceLevels.length; i < j; i++) {
      experienceLevels[i].classList.remove('enabled');
    }
    addEventExperienceLevel.value = value;
    validate();
  }

  addEventTime.addEventListener('select', validate);
  addEventDate.addEventListener('change', validate);
  addEventDate.addEventListener('keyup', validate);
  addEventEventType.addEventListener('select', validate);
  addEventLocation.addEventListener('change', validate);
  addEventLocation.addEventListener('keyup', validate);
  addEventDescription.addEventListener('change', validate);
  addEventDescription.addEventListener('keyup', validate);

  var experienceLevels = document.getElementById('addEventView').querySelectorAll('.addEvent-experience-level');
  for (var i = 0, j = experienceLevels.length; i < j; i++) {
    var experienceLevel = experienceLevels[i];
    experienceLevel.addEventListener('click', selectExperienceLevel.bind(experienceLevel));
  }

  return {
    submit: function () {
      if (!this.getAttribute('disabled')) {
        var startDatetime = addEventDate.value,
            startTimeMatch = addEventTime.value.match(/(\d{1,2}):(\d{2})(am|pm)/);
        if (startTimeMatch) {
          var hour = parseInt(startTimeMatch[1], 10);
          if (startTimeMatch[3].toUpperCase() === 'PM') {
            hour += 12;
          } else if (hour < 10) {
            hour = '0' + hour;
          }
          startDatetime += ' ' + hour + ':' + startTimeMatch[2] + ':00';
          Ajax.request({
            url: 'https://secure22.win.hostgator.com/api_baddtech_com/AppServices.asmx/CreateEvent',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            data: jsonEncode({
              _token: localStorage.getItem('token'),
              _location: addEventLocation.value,
              _startdatetime: startDatetime,
              _enddatetime: startDatetime,
              _runtype: addEventEventType.value,
              _city: 'Edmonton',
              _exp: addEventExperienceLevel.value,
              _country: 'Canada',
              _description: addEventDescription.value,
            }),
            json: true,
            processData: false,
          }).done(function (response) {
            console.log(response);
            BallTogether.routing.go('home');
          }).fail(function (response) {
            console.error(response);
          });
        }
      }
    },
  };
});
