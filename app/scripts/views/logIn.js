BallTogether.module('BallTogether.views.logIn', function () {
  'use strict';

  var logInEmail = document.getElementById('logInEmail'),
      logInPassword = document.getElementById('logInPassword'),
      logInSubmit = document.getElementById('logInSubmit');

  function disableSubmit() {
    logInSubmit.setAttribute('disabled', true);
  }
  function enableSubmit() {
    logInSubmit.removeAttribute('disabled');
  }

  function validate() {
    if (!/^.+@.+$/.test(logInEmail.value)) {
      disableSubmit();
    } else if (!logInPassword.value.length) {
      disableSubmit();
    } else {
      enableSubmit();
    }
  }

  logInEmail.addEventListener('keyup', validate);
  logInPassword.addEventListener('keyup', validate);

  return {
    submit: function () {
      if (!this.getAttribute('disabled')) {
        Ajax.request({
          url: 'https://secure22.win.hostgator.com/api_baddtech_com/AppServices.asmx/ValidateUser',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          data: jsonEncode({
            _userName: logInEmail.value,
            _pass: logInPassword.value,
          }),
          json: true,
          processData: false,
        }).done(function (response) {
          var d = response.d;
          if (d && d.ResponseCode === 0) {
            var token = d.ResponseData;
            if (token) {
              localStorage.setItem('token', token);
              BallTogether.routing.go('home');
            } else {
              console.error('login failed:', response);
            }
          } else {
            console.error('login failed:', response);
          }
        }).fail(function (response) {
          console.error('login failed:', response);
        });
      }
    },
  };
});
