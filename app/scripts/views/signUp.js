BallTogether.module('BallTogether.views.signUp', function () {
  'use strict';

  var signUpEmail = document.getElementById('signUpEmail'),
      signUpPassword = document.getElementById('signUpPassword'),
      signUpConfirmPassword = document.getElementById('signUpConfirmPassword'),
      signUpAge = document.getElementById('signUpAge'),
      signUpGender = document.getElementById('signUpGender'),
      signUpCity = document.getElementById('signUpCity'),
      signUpCountry = document.getElementById('signUpCountry'),
      signUpSubmit = document.getElementById('signUpSubmit');

  function disableSubmit() {
    signUpSubmit.setAttribute('disabled', true);
  }
  function enableSubmit() {
    signUpSubmit.removeAttribute('disabled');
  }

  function validate() {
    if (!/^.+@.+$/.test(signUpEmail.value)) {
      disableSubmit();
    } else if (!signUpPassword.value.length) {
      disableSubmit();
    } else if (signUpPassword.value !== signUpConfirmPassword.value) {
      disableSubmit();
    } else if (!signUpAge.value.length) {
      disableSubmit();
    } else if (!signUpGender.value.length) {
      disableSubmit();
    } else if (!signUpCity.value.length) {
      disableSubmit();
    } else if (!signUpCountry.value.length) {
      disableSubmit();
    } else {
      enableSubmit();
    }
  }

  signUpPassword.addEventListener('keyup', validate);
  signUpConfirmPassword.addEventListener('keyup', validate);
  signUpAge.addEventListener('select', validate);
  signUpGender.addEventListener('select', validate);
  signUpEmail.addEventListener('keyup', validate);
  signUpCity.addEventListener('select', validate);
  signUpCountry.addEventListener('select', validate);

  function login() {
    Ajax.request({
      url: 'https://secure22.win.hostgator.com/api_baddtech_com/AppServices.asmx/ValidateUser',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: jsonEncode({
        _userName: signUpEmail.value,
        _pass: signUpPassword.value,
      }),
      json: true,
      processData: false,
    }).done(function (response) {
      var d = response.d;
      if (d && d.ResponseCode === 0) {
        var token = d.ResponseData;
        if (token) {
          localStorage.setItem('token', token);
          BallTogether.routing.go('home');
        } else {
          console.error('initial login failed:', response);
        }
      } else {
        console.error('initial login failed:', response);
      }
    }).fail(function (response) {
      console.error('initial login failed:', response);
    });
  }

  return {
    submit: function () {
      if (!this.getAttribute('disabled')) {
        Ajax.request({
          url: 'https://secure22.win.hostgator.com/api_baddtech_com/AppServices.asmx/CreateUser',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          data: jsonEncode({
            _gender: signUpGender.value,
            _username: signUpEmail.value,
            _password: signUpPassword.value,
            _email: signUpEmail.value,
            _city: signUpCity.value,
            _country: signUpCountry.value,
            _age: signUpAge.value,
            _position: 'F',
            _skilllevel: 5,
          }),
          json: true,
          processData: false,
        }).done(function (response) {
          var d = response.d;
          if (d && d.ResponseCode === 0) {
            login();
          } else {
            console.error('account creation failed:', response);
          }
        }).fail(function (response) {
          console.error('account creation failed:', response);
        });
      }
    },
  };
});
