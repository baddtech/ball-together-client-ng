BallTogether.module.waitFor([
  'BallTogether.ripple',
  'BallTogether.routing',
], function () {
  'use strict';

  BallTogether.ripple.bind(document);
  BallTogether.input.bind(document);
  BallTogether['select-input'].bind(document);

  if (localStorage.getItem('token') !== null) {
    BallTogether.routing.go('home');
  }
});
